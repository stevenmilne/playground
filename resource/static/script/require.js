(function () {

	const cache = {};
	const handlers = {};

	class Module {
		constructor({ id, paths, filename, parent = null, loaded = false, children = [], exports = {} }) {
			this.filename = filename || id;
			this.children = children;
			this.exports = exports;
			this.parent = parent;
			this.loaded = loaded;
			this.paths = paths;
			this.id = id || filename;
		}

		async require(id) {
			return await require(id, this);
		}

		getRequire() {
			return this.require.bind(this);
		}
	}

	/**
	 * @private
	 * Loads the text from a given URL.
	 * @param {string} url The URI of the resource to load.
	 * @return {string} The string content of the resource.
	 */
	async function load(url) {
		const request = await fetch(url);
		
		if (request.status !== 200) {
			const error = new Error(`Failed to load module "${url}" (${request.status} ${request.statusText}).`);
			error.request = request;

			throw error;
		}

		return await request.text();
	}

	/**
	 * @private
	 * Determines the directory name of the given URL. The directory is interperated
	 * as the whole path minus the last `/` part.
	 * @param {string} url The URL to parse a directory name from
	 * @return {string} The directory name.
	 */
	function dirname(url) {
		const matches = url.match(/^(\/.+)\/.+$/);

		return matches[1];
	}

	/**
	 * @private
	 * Determines the file extension from the URL, for example `/script/require.js`
	 * would return `.js`.
	 * @param {string} url The URL to parse a file extension from.
	 * @return {string} The file extension or an empty string
	 */
	function extension(url) {
		const matches = url.match(/\/.+\.(.+)?$/);

		return matches && matches.length > 1 ? matches[1] : '';
	}

	function getRegister(url, parent) {
		return module => {
			if (module.loaded !== undefined) module.loaded = true;

			cache[url] = module;
			require.modules.push(module);

			if (parent) {
				parent.children.push(module);
			}

			return module.exports || module;
		}
	}

	/**
	 * Loads a CommonJS module and returns the module exports. Javascript files
	 * loaded by this function will be wrapped to add extra variables to its scope.
	 * The added variables are `exports`, `require`, `module`, `__filename`,
	 * `__dirname` and `global`.
	 * @param {string} url The URL of the CommonJS module to load.
	 * @param {Module} [parent=null] The module that is doing the require.
	 * @return {Promise<Object>} The exports of the required module.
	 */
	function require(url, parent = null) {
		const ext = extension(url);
		if (!ext) {
			url = `${url}.js`;
		}

		if (cache[url]) {
			if (cache[url] instanceof Promise) {
				cache[url].then( () => {
					if (parent && !parent.children.includes(cache[url])) {
						parent.children.push(cache[url]);
					}
				});
				return cache[url];
			}

			if (parent && !parent.children.includes(cache[url])) {
				parent.children.push(cache[url]);
			}
			return cache[url].exports;
		}

		cache[url] = new Promise((resolve, reject) => {
			const register = getRegister(url, parent);
			for(let filetype in handlers) {
				if (url.endsWith(filetype)) {
					handlers[filetype](url, register).then( exports => {
						resolve(exports);
					})
					return;
				}
			}

			load(url).then( body => {
				const module = new Module({
					parent: parent,
					paths: [dirname(url)],
					filename: url
				});
	
				invoke(body, module).then( () => {
					resolve(register(module));
				});
			});
		});

		return cache[url];
	}

	/**
	 * @private
	 * Converts the given module source into an executable module. This function
	 * should shadow all variables from outer scopes from the inner module code.
	 * @param {string} source The Javascript module source code.
	 * @param {Module} module The module instance to be injected into the source.
	 */
	async function invoke(source, module) {																							 // Extra arguments here to shadow the local scope
		const fn = (async function (exports, require, module, __filename, __dirname, global,    invoke, dirname, load, cache, extension, requireWasm, handlers) {
			const instance = eval(`async () => {${source}}`);
			await instance();
		});

		fn.bind(module);
		await fn(module.exports, module.getRequire(), module, module.filename, module.paths[0], self);
	}

	require.Module = Module;
	require.modules = [];
	require.cache = cache;

	/**
	 * @chainable
	 * Installs a handler function for a specific file type extension. This handler
	 * will be invoked for all require calls which have the specified extension.
	 *
	 * Handlers must call the provided `register(url, module)` function once they
	 * have instantiated a module. This registration adds the module instance to
	 * the cache and also relates the module it's parent module.
	 * @param {string} filetype The file type extension to handle calls for.
	 * @param {Function} handler The handler to be invoked.
	 * @param {string} handler.url The URL that has been requested.
	 * @param {Function} handler.register Registers the provided module in the cache
	 */
	require.handle = function handle(filetype, handler) {
		handlers[filetype] = handler;

		return this;
	}

  require.handleAll = function handle(filetypes, handler) {
    filetypes.map( filetype => require.handle(filetype, handler) );

    return this;
  }

	self.require = require;

	if (self.document && document.currentScript && document.currentScript.dataset.boot) {
		const bootUrl = document.currentScript.dataset.boot;

		window.addEventListener('load', () => {
			require(bootUrl);
		}, { once: true })
	}
})();
