
exports.AbstractWebAssemblyModule = class AbstractWebAssemblyModule {

	/**
	 * Constructs a new Web Assembly module abstraction.
	 * @param {string} url The URL of the .wasm file to manage.
	 * @param {Object} config Configuration overrides.
	 * @return {AbstractWebAssemblyModule} The new instance.
	 */
	constructor(url, config) {
		if (!url) {
			throw new Error('Must provide a URL to load the WebAssembly module from.');
		}

		this.url = url;
		this.loaded = false;
		this.imports = {
			env: {}
		};
	}

	/**
	 * @chainable
	 * Imports the provided function into web assembly module. This can only be
	 * called before the module has been loaded.
	 * @param {string} name The name of the imported function.
	 * @param {Function} fn The function to be imported.
	 * @return {AbstractWebAssemblyModule} This instance to chain call from.
	 */
	provide(name, fn) {
		if (this.loaded) throw new Error('Cannot import into a running module.');

		this.imports.env[name] = fn;

		return this;
	}

	/**
	 * @chainable
	 * Loads and instantiates the wasm module.
	 */
	async load() {
		if (this.loaded) return;

		this.module = await require(this.url);
		this.loaded = true;

		this.init();
		await this.instantiate();
		this.extractExports();
	}

	/**
	 * @private
	 * Initialises the environment for the wasm.
	 */
	init() {
		if (this.memory || this.table) {
			throw new Error('Cannot initialise the module more than once.');
		}

		this.memory = new WebAssembly.Memory({ initial: 1 });
		this.table = new WebAssembly.Table({ initial: 10, element: 'funcref' });

		this.imports.js = {
			memory: this.memory,
			table: this.table
		};
	}

	/**
	 * @private
	 * Instantiates the module.
	 */
	async instantiate() {
		if (!this.module) throw new Error('Cannot instantiate an uncompiled module.');
		if (this.instance) throw new Error('Module already instantiated.');

		this.instance = await WebAssembly.instantiate(this.module, this.imports);
	}

	/**
	 * @private
	 * Extracts all of the exported functions from the instance and applies them
	 * to this class instance.
	 */
	extractExports() {
		const exports = this.instance.exports;

		for (let name in exports) {
			if (name === 'memory') continue;

			this[name] = exports[name];
		}
	}
}
