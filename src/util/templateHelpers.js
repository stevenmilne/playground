const Hogan = require('hogan.js');
const Config = require('../../config.js');
const fs = require('fs');

exports.template = function template(name, data = {}) {
  const filename = `${Config.TEMPLATE_DIR}/${name}.${Config.TEMPLATE_EXT}`;
  const content = fs.readFileSync(filename);
  const template = Hogan.compile(content.toString());

  return template.render(data);
}
