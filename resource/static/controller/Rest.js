const { xhr } = await require('/module/util.js');

const API_MESSAGE = '/api/message'
const HTTP_GET = 'GET';
const HTTP_PUT = 'PUT';
const HTTP_DELETE = 'DELETE';

class RestController {
	constructor() {
		this.elements = {
			message: document.querySelector('#message'),
			get: document.querySelector('#submitGet'),
			put: document.querySelector('#submitPut'),
			delete: document.querySelector('#submitDelete'),
			log: document.querySelector('#log')
		};

		this.elements.get.addEventListener('click', this.onMessageGet.bind(this));
		this.elements.put.addEventListener('click', this.onMessagePut.bind(this));
		this.elements.delete.addEventListener('click', this.onMessageDelete.bind(this));
	}

	async onMessageGet(event) {
		event.preventDefault();

		this.log(`GET ${API_MESSAGE} HTTP/1.1\n`);
		const content = await xhr(API_MESSAGE, HTTP_GET);
		this.elements.message.value = content;
		this.log(`HTTP/1.1 200 OK\n`);

	}

	async onMessagePut(event) {
		event.preventDefault();

		const content = this.elements.message.value;
		this.log(`PUT ${API_MESSAGE} HTTP/1.1\n`);
		await xhr(API_MESSAGE, HTTP_PUT, content);
		this.log(`HTTP/1.1 200 OK\n`);
	}

	async onMessageDelete(event) {
		event.preventDefault();

		this.log(`DELETE ${API_MESSAGE} HTTP/1.1\n`);
		await xhr(API_MESSAGE, HTTP_DELETE);
		this.elements.message.value = '';
		this.log(`HTTP/1.1 200 OK\n`);
	}

	log(message) {
		this.elements.log.innerText += message;
	}
}

new RestController();
