const { AbstractController } = require('@smilne/router');
const { HttpStatus, contentType, contentLength } = require('@smilne/http-utils');
const { FileSystem, Logger } = require('@smilne/util');

const MULTIPART_BOUNDARY = 'MULTIPART-BOUNDARY--'

class RangeController extends AbstractController {

	constructor(filename, type) {
		super();

		this.filename = filename;
		this.type = type;
	}

	async get(request) {
		const range = request.getHeader('range');
		const size = await this.getLength();

		if (!range) {
			return await this.streamAll(request, size);
		}

		const isMultipart = range.indexOf(',') >= 0;
		if (!isMultipart) {
			Logger.getLogger().info('  Streaming single range');
			// TODO -- Get range and respond
			return;
		}

		// TODO -- Get ranges and respond with them
		Logger.getLogger().info('  Streaming multiple ranges');
	}

	async head(request) {
		const range = request.getHeader('range');
		const isMultipart = range && request.getHeader('range').indexOf(',') >= 0;

		request.respond(null, HttpStatus.NO_CONTENT, {
			...(isMultipart
				? contentType(`multipart/byteranges; boundary=${MULTIPART_BOUNDARY}`)
				: contentType(this.type)
			),
			'Content-Length': await this.getLength(),
			'Accept-Ranges': 'bytes'
		});
	}

	async getLength() {
		return await FileSystem.size(this.filename);
	}

	async streamAll(request, size) {
		request.stream(
			FileSystem.stream.read(this.filename),
			HttpStatus.OK,
			{
				...contentType(this.type),
				// ...contentLength(size),
				'Accept-Ranges': 'bytes'
			}
		);
	}

}

module.exports = RangeController;
