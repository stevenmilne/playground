const Config = require('../../config.js');
const { FileSystem } = require('@smilne/util');

const FILENAME = Config.DATA_DIR + '/message.txt';

class MessageService {

	async get() {
		try {
			const data = await FileSystem.read(FILENAME);

			return data.toString();
		} catch(error) {
			console.log(error);
			return '';
		}
	}

	async put(content) {
		await FileSystem.write(FILENAME, content);
	}

	async delete() {
		await FileSystem.delete(FILENAME);
	}

}

module.exports = new MessageService();
