const { toHex } = await require('/module/util');
const { ChunkType } = await require('/module/png/chunk/Chunk.js');
const RawChunk = await require('/module/png/chunk/RawChunk.js');
const ImageDataChunk = await require('/module/png/chunk/ImageDataChunk.js');

await require('/module/png/TypedChunks.js');

const PNG_HEADER_LENGTH = 8;
const PNG_HEADER = '89 50 4e 47 0d 0a 1a 0a';
const PNG_ASCII = '50 4e 47';

const CHUNK_HEADER_LENGTH = 12;

module.exports = class PNG {

  /**
   * @static
   * Decodes a PNG image from the given blob. Throws an exception if
   * the blob does not contain PNG data or if it is corrupt.
   * @param {Blob} blob The blob to decode.
   * @param {Object} options Options to pass to the decoder.
   * @return {Promise<PNG>} The decoded PNG image.
   */
  static async fromBlob(blob, options = {}) {
    performance.mark('StartDecodeBlock');
    const buffer = await blob.arrayBuffer();
    validate(buffer);

    const png = new PNG(options);
    await png.decodeChunks(
      extractChunks(buffer)
    );

    performance.mark('EndDecodeBlock');
    performance.measure('Decode PNG Blob', 'StartDecodeBlock', 'EndDecodeBlock');
    return png;
  }

  constructor({ noChecksum = false }) {
    this.chunks = {};

    this.options = {
      noChecksum
    };

    if (noChecksum) {
      console.warn('PNG checksum validation switched off with { noChecksum: true } option.');
    }
  }

  /**
   * Gets the height of the PNG image.
   * @return {number} The height of the image.
   */
  get height() {
    const header = this.chunks[ChunkType.IHDR];

    return header ? header.height : 0;
  }

  /**
   * Gets the width of the PNG image.
   * @return {number} The width of the image.
   */
  get width() {
    const header = this.chunks[ChunkType.IHDR];

    return header ? header.width : 0;
  }

  /**
   * Gets an array of scanlines in the image. Each scanline is an array
   * of pixel data.
   * @return {Pixel[][]} The pixels in the image grouped by scanline.
   */
  get scanlines() {
    const image = this.chunks[ChunkType.IDAT];

    return image ? image.scanlines : [];
  }

  /**
   * Gets an array of pixel data.
   * @return {Pixel[]} All of the pixels in the image.
   */
  get pixels() {
    return this.scanlines.flat();
  }

  /**
   * Gets the image data in a format ready to be rendered.
   * @return {ImageData} The image data.
   */
  get image() {
    const pixels = this.pixels;
    const buffer = new Uint8ClampedArray(pixels.length * 4);

    for (let i = 0; i < pixels.length; i++) {
      const pointer = i * 4;

      buffer[pointer] = pixels[i].red;
      buffer[pointer + 1] = pixels[i].green;
      buffer[pointer + 2] = pixels[i].blue;
      buffer[pointer + 3] = pixels[i].alpha;
    }

    return new ImageData(buffer, this.width, this.height);
  }

  /**
   * Adds the given chunk to the PNG data.
   * @param {Chunk} chunk The chunk to be added.
   */
  addChunk(chunk) {
    if (!chunk.isStackable()) {
      this.chunks[chunk.type] = chunk;
      return
    }

    if (chunk.getKey) {
      if (!this.chunks[chunk.type]) {
        this.chunks[chunk.type] = {};
      }

      this.chunks[chunk.type][chunk.getKey()] = chunk;
    } else {
      if (!this.chunks[chunk.type]) {
        this.chunks[chunk.type] = [];
      }

      this.chunks[chunk.type].push(chunk);
    }
  }

  /**
   * Encodes the current PNG data into a blob.
   * @throws {Error} When data is missing critical chunks.
   * @return {Blob} The encoded PNG data.
   */
  async encode() {
    // TODO -- Implement
  }

  /**
   * @private
   * Reads the data from the given chunk and deserialises it.
   * @param {RawChunk[]} chunk The chunk to be deserialised.
   */
  async decodeChunks(chunks) {
    const data = [];

    for (let i = 0; i < chunks.length; i++) {
      const chunk = chunks[i];

      try {
        if (!this.options.noChecksum) {
          chunk.validate();
        }
      } catch(error) {
        if (!chunk.isOptional) {
          throw error;
        }

        console.warn(`Optional chunk ${chunk.type} failed validation, it will be ignored.`);
        this.addChunk(chunk);
        continue;
      }

      if (chunk.type === ChunkType.IDAT) {
        data.push(chunk);
        continue;
      }

      const decodedChunk = await chunk.decode();
      this.addChunk(decodedChunk);
    }

    this.chunks[ChunkType.IDAT] = await ImageDataChunk.decode(this.chunks[ChunkType.IHDR], data);
  }
}

/**
 * @private
 * Checks the file header to ensure it is a PNG file.
 * @param {ArrayBuffer} buffer The file buffer.
 */
function validate(buffer) {
  const header = new Uint8Array(buffer, 0, PNG_HEADER_LENGTH);
  const bytes = Array.from(header);
  const magic = toHex(bytes.slice(1, 4));
  const hex = toHex(bytes);

  if (magic !== PNG_ASCII) {
    throw new Error('Blob does not contain PNG data');
  }

  if (hex !== PNG_HEADER) {
    throw new Error('PNG data is corrupt');
  }
}

/**
 * @private
 * Extracts chunks of data from the PNG file and returns them.
 * @param {ArrayBuffer} buffer The file buffer to extract chunk data from.
 * @return {RawChunk[]} An array of all chunks contained within the file.
 */
function extractChunks(buffer) {
  const chunks = [];

  let pointer = PNG_HEADER_LENGTH;
  while (pointer < buffer.byteLength) {
    const chunk = RawChunk.fromBytes(buffer, pointer);

    chunks.push(chunk);

    pointer = pointer + chunk.length + CHUNK_HEADER_LENGTH;
  }

  return chunks;
}