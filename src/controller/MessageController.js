const { AbstractController } = require('@smilne/router');
const { HttpStatus } = require('@smilne/http-utils');
const MessageService = require('../service/MessageService.js');

class MessageController extends AbstractController {

	async get(request) {
		const message = await MessageService.get();

		request.respondText(message);
	}

	async put(request) {
		const body = await request.getBody();

		await MessageService.put(body);
		request.respondText(body);
	}

	async delete(request) {
		await MessageService.delete();

		request.respond(null, HttpStatus.NO_CONTENT);
	}

}

module.exports = new MessageController();
