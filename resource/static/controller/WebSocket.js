
const ChatController = await require('/module/ChatController.js');
const controller = new ChatController(document);

const textSocket = new WebSocket('ws://localhost:3000/echo', 'plaintext');
const jsonSocket = new WebSocket('ws://localhost:3000/echo', 'json+schema');
const xmlSocket = new WebSocket('ws://localhost:3000/echo', 'xml+schema');

const json = message => `{ "message": "${message}" }`;
const xml = message => `<?xml version="1.0" encoding="UTF-8"?>
<body>
	<message>${message}</message>
</body>`;

function onMessage(event) {
	console.log('Message received: ', event.data);
}

function onOpen(socket, message, message2) {
	return event => {
		socket.send(message);
		setTimeout(() => socket.send(message2), 500);
	}
}

textSocket.onmessage = onMessage;
textSocket.onopen = onOpen(textSocket, 'schema', 'Hello world!');

jsonSocket.onmessage = onMessage;
jsonSocket.onopen = onOpen(jsonSocket, json('schema'), json('Hello world!'));

xmlSocket.onmessage = onMessage;
xmlSocket.onopen = onOpen(xmlSocket, xml('schema'), xml('Hello world!'));
