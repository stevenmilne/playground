const InternationalTextChunk = await require('/module/png/chunk/InternationalTextChunk.js');
const CompressedTextChunk = await require('/module/png/chunk/CompressedTextChunk.js');
const AspectRatioChunk = await require('/module/png/chunk/AspectRatioChunk.js');
const HeaderChunk = await require('/module/png/chunk/HeaderChunk.js');
const TextChunk = await require('/module/png/chunk/TextChunk.js');
const TimeChunk = await require('/module/png/chunk/TimeChunk.js');

module.exports = {
  InternationalTextChunk,
  CompressedTextChunk,
  AspectRatioChunk,
  HeaderChunk,
  TextChunk,
  TimeChunk
};