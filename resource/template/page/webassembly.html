<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Web Assembly</title>
		{{{includes}}}

		<script type="text/javascript" src="/script/require.js" data-boot="/controller/WebAssembly.js"></script>
		<script type="text/javascript" src="/script/require-extensions.js"></script>
	</head>
	<body class="font-helvetica narrow">
		<header>
			{{{header}}}
		</header>
		<main>
			<article>
				<h2>Web Assembly</h2>
				<p>
					Web Assembly is a Web API which allows compiled machine code to be run on the web. Web assembly implements a virtual stack based processor which compilers and assemblers can target. Currently Web assembly supports running code written in C/C++ and also Rust. Web Assembly binary files usually have the file extension <code>.wasm</code> and must be loaded using the Javascript API. Web assembly can provide performance improvements over Javascript since it's executed as machine code on a virtual machine as opposed to the interpreted nature of Javascript. Web assembly can be called <i>wasm</i> for short.
				</p>

				<p>
					Web assembly can also be written directly as <code>.wat</code> files. These files contain assembly language that must be assembled before they can be executed. Web assembly is different to normal assembly since the virtual processor is stack based, not register based. Also wasm assembly directly supports concepts like functions and for loops. Textual web assembly is represented using S-Expressions.
				</p>

				<p>
					The web assembly API consists of a couple of new classes which allow Javascript to interact with the wasm code. The wasm code itself is compiled into relocatable modules represented by the
					<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WebAssembly/Module">
						<code>WebAssembly.Module</code></a>
					class. These classes can be shared between different executions contexts such as documents, workers and shared workers. To actually run the module it must first be instantiated into a
					<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WebAssembly/Instance">
						<code>WebAssembly.Instance</code></a>
					class. This class contains the module code and the state require for it to run. Functions, memory and tables can be imported and exported from the instance at instantiation time.
				</p>

				<p>
					Below is an example of Javascript loading and running a wasm module which prints "Hello world, from WASM!" to the developer console;
				</p>

				<pre class="code-block with-output"><code>(module
  (import "env" "log" (func $log (param i32 i32)))
  (import "js" "mem" (memory 1))
  (data (i32.const 0) "Hello world, from WASM!")
  (func (export "hello")
    i32.const 0  ;; pass offset 0 to log
    i32.const 23  ;; pass length 23 to log
    call $log
  )
)</code></pre>
			<pre class="code-block with-above"><code>const response = await fetch(url);
const bytes = await response.arrayBuffer();
const module =  await WebAssembly.compile(bytes);

const memory = new WebAssembly.Memory({ initial: 1 });
const instance = await WebAssembly.instantiate(module, {
  js: { mem: memory },
  env: {
    log: (offset, length) => {
      const bytes = new Uint8Array(memory.buffer, offset, length);

      console.log(new TextDecoder().decode(bytes));
    }
  }
});

instance.exports.hello();</code></pre>

				<p>
					In the example the Javascript had to request the wasm file as an
					<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer">
						<code>ArrayBuffer</code></a>
					and compile it. This process turns the wasm byte-code into machine code for the wasm virtual processor. The Javascript then created an instance of
					<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WebAssembly/Memory">
						<code>WebAssembly.Memory</code></a>
					which is then imported into the final wasm instance as a way of sharing an address space. The Javascript also imports a function called <code>log</code> which the wasm code can call.
				</p>

				<p>
					The wasm code imports the <code>log</code> function from Javascript and applies a type to it (web assembly is type safe!). It also imports a 64kb page of memory from Javascript. The wasm then defines a constant at location 0 in the imported memory with the message to be logged. The <code>hello</code> function simply pushes the offset and length of the constant onto the stack, then calls the imported <code>log</code> function. The imported function will implicitly pull it's arguments from the stack.
				</p>

				<h3>Opcodes</h3>
				<p>
					The wasm processor supports a variaty of typed and type agnostic op codes. The processor supports only numeric types but of different sizes, the supported types are;
				</p>

				<ul>
					<li><code>i32</code> - 32 bit integer</li>
					<li><code>i64</code> - 64 bit integer</li>
					<li><code>f32</code> - 32 bit float</li>
					<li><code>f64</code> - 64 bit float</li>
				</ul>

				<p>
					Note that the type is only concerned with the size of the stored value, the sign (positive or negative) is not concidered part of the type. There are opcodes that only exist for integer values and opcodes which exist only for float values. The opcodes usually take the form of <code>type.opcode</code>, or just <code>opcode</code> when it is type indifferent.
				</p>

				<h4>Load & Store</h4>
				<p>
					Wasm supports instructions for loading values from linear memory onto the stack and storing values from the stack into linear memory. The code can load smaller values than the supported types however this extends them up to a selected size. Below is a list of all the the load instructions;
				</p>

				<ul>
					<li><code>i32.load8_s</code>: load 1 byte and sign-extend i8 to i32</li>
					<li><code>i32.load8_u</code>: load 1 byte and zero-extend i8 to i32</li>
					<li><code>i32.load16_s</code>: load 2 bytes and sign-extend i16 to i32</li>
					<li><code>i32.load16_u</code>: load 2 bytes and zero-extend i16 to i32</li>
					<li><code>i32.load</code>: load 4 bytes as i32</li>
					<li><code>i64.load8_s</code>: load 1 byte and sign-extend i8 to i64</li>
					<li><code>i64.load8_u</code>: load 1 byte and zero-extend i8 to i64</li>
					<li><code>i64.load16_s</code>: load 2 bytes and sign-extend i16 to i64</li>
					<li><code>i64.load16_u</code>: load 2 bytes and zero-extend i16 to i64</li>
					<li><code>i64.load32_s</code>: load 4 bytes and sign-extend i32 to i64</li>
					<li><code>i64.load32_u</code>: load 4 bytes and zero-extend i32 to i64</li>
					<li><code>i64.load</code>: load 8 bytes as i64</li>
					<li><code>f32.load</code>: load 4 bytes as f32</li>
					<li><code>f64.load</code>: load 8 bytes as f64</li>
				</ul>

				<p>
					The storage commands also support shrinking the size of types back down to smaller sizes. Below is a list of the storage instructions;
				</p>

				<ul>
					<li><code>i32.store8</code>: wrap i32 to i8 and store 1 byte</li>
					<li><code>i32.store16</code>: wrap i32 to i16 and store 2 bytes</li>
					<li><code>i32.store</code>: (no conversion) store 4 bytes</li>
					<li><code>i64.store8</code>: wrap i64 to i8 and store 1 byte</li>
					<li><code>i64.store16</code>: wrap i64 to i16 and store 2 bytes</li>
					<li><code>i64.store32</code>: wrap i64 to i32 and store 4 bytes</li>
					<li><code>i64.store</code>: (no conversion) store 8 bytes</li>
					<li><code>f32.store</code>: (no conversion) store 4 bytes</li>
					<li><code>f64.store</code>: (no conversion) store 8 bytes</li>
				</ul>

				<h4>Flow Control</h4>

				<ul>
					<li><code>nop</code>: no operation, no effect</li>
					<li><code>block</code>: the beginning of a block construct, a sequence of instructions with a label at the end</li>
					<li><code>loop</code>: a block with a label at the beginning which may be used to form loops</li>
					<li><code>if</code>: the beginning of an if construct with an implicit then block</li>
					<li><code>else</code>: marks the else block of an if</li>
					<li><code>br</code>: branch to a given label in an enclosing construct</li>
					<li><code>br_if</code>: conditionally branch to a given label in an enclosing construct</li>
					<li><code>br_table</code>: a jump table which jumps to a label in an enclosing construct</li>
					<li><code>return</code>: return zero or more values from this function</li>
					<li><code>end</code>: an instruction that marks the end of a block, loop, if, or function</li>
				</ul>
			</article>
		</main>
	</body>
</html>
