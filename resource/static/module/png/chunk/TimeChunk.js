const { readFields, NumberReader } = await require('/module/png/FieldReader.js');
const { Chunk, ChunkType } = await require("/module/png/chunk/Chunk.js");
const RawChunk = await require("/module/png/chunk/RawChunk.js");

class TimeChunk extends Chunk {
  static decode(chunk) {
    chunk.assert(ChunkType.tIME);

    const [ year, month, day, hour, minute, second ] = readFields(chunk.bytes, [
      new NumberReader(2),
      new NumberReader(),
      new NumberReader(),
      new NumberReader(),
      new NumberReader(),
      new NumberReader()
    ]);

    return new TimeChunk(chunk.type, { year, month, day, hour, minute, second });
  }

  constructor(type, { year, month, day, hour, minute, second }) {
    super(type);

    this.year = year;
    this.month = month;
    this.day = day;
    this.hour = hour;
    this.minute = minute;
    this.second = second;

    this.date = new Date(year, month - 1, day, hour, minute, second);
  }
};

RawChunk.register(ChunkType.tIME, TimeChunk);
module.exports = TimeChunk;