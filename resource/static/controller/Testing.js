const PNG = await require('/module/PNG.js');

class Person {
  constructor({ name, age } = {}) {
    this.name = name;
    this.age = age;
  }
}

class TestingController {
  fileEl = null;
  pngEl = null;
  canvasEl = null;

  constructor() {
    this.fileEl = document.querySelector('#file');
    this.pngEl = document.querySelector('#pngFile');
    this.canvasEl = document.querySelector('#pngCanvas');

    this.fileEl.addEventListener('change', this.handleFileChange.bind(this));
    this.pngEl.addEventListener('change', this.handlePngChange.bind(this));
  }

  async handlePngChange(event) {
    const file = event.target.files[0];
    const png = await PNG.fromBlob(file, { noChecksum: true });

    console.log(png);

    document.querySelector('#pngExample').src = URL.createObjectURL(file);

    this.canvasEl.height = png.height;
    this.canvasEl.width = png.width;
    const context = this.canvasEl.getContext('2d');

    context.putImageData(png.image, 0, 0);

    document.querySelector('#pngResult').style = "";
  }

  async handleFileChange(event) {
    const file = event.target.files[0];

    console.log(file);

    const content = await this.readFile(file);
    const lines = content.split('\n').splice(1);

    for (const line of lines) {
      const [ name, age ] = line.split(',');

      console.log(new Person({ name, age }));
    }
  }

  readFile(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.addEventListener('load', event => {
        resolve(event.target.result);
      });

      reader.addEventListener('error', event => {
        reject(event);
      })

      reader.readAsText(file);
    });
  }
}

module.exports = new TestingController();
