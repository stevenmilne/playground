const { server } = require('@smilne/server');
const fs = require('fs');

const Config = require('./config.js');
const App = require('./src/App.js');

server(new App(), {
	httpPort: Config.HTTP_PORT,
	httpsPort: Config.HTTPS_PORT,
	staticDir: Config.STATIC_DIR,
	logFile: Config.LOG_FILE,
	withSocket: true,
	ssl: {
		key: fs.readFileSync(Config.SSL_KEY),
		cert: fs.readFileSync(Config.SSL_CERT),
		requestCert: false,
	  rejectUnauthorized: false
	}
});
