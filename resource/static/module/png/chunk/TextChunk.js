const { Chunk, ChunkType } = await require('/module/png/chunk/Chunk.js');
const RawChunk = await require('/module/png/chunk/RawChunk.js');

const Keyword = {
  Title: 'Title',
  Author: 'Author',
  Description: 'Description',
  Copyright: 'Copyright',
  CreationTime: 'Creation Time',
  Software: 'Software',
  Disclaimer: 'Disclaimer',
  Warning: 'Warning',
  Source: 'Source',
  Comment: 'Comment'
};

class TextChunk extends Chunk {

  static Keyword = Keyword;

  static decode(chunk) {
    chunk.assert(ChunkType.tEXt);

    const bytes = new Uint8Array(chunk.bytes);
    const [ key, value ] = new TextDecoder().decode(bytes).split('\0');

    return new TextChunk(chunk.type, { key, value });
  }

  constructor(type, { key, value }) {
    super(type);

    this.key = key;
    this.value = value;
  }

  /**
   * @override
   * This function is implemented for typed chunks and returns a flag
   * which specifies if multiple chunks in the same file are supported.
   * @return {boolean} True if this chunk type supports duplicate chunks.
   */
  isStackable() {
    return true;
  }

  /**
   * @override
   * This function is implemented for stackable typed chunks. Chunks which
   * support being mapped by a keyword can implement this function to return
   * the unique keyword that can identify a chunk.
   * @return {string} A value that can map this chunk instance.
   */
  getKey() {
    return this.key;
  }
}

RawChunk.register(ChunkType.tEXt, TextChunk);
module.exports = TextChunk;