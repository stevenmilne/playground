const { Chunk, ChunkType } = await require('/module/png/chunk/Chunk.js');
const { crc32 } = await require('/module/png/crc.js');

module.exports = class RawChunk extends Chunk {

  static CHUNK_CLASS_BY_TYPE = {};
  static NOT_DECODABLE = [ChunkType.IEND];

  /**
   * Registers a new chunk decoder for the specified type. This allows
   * raw chunks of the type to be decoded with the decode() instance
   * method.
   * @param {ChunkType|string} type The chunk type to register for decoding.
   * @param {Chunk} Decoder The decoder class constructor to use for decoding.
   */
  static register(type, Decoder) {
    RawChunk.CHUNK_CLASS_BY_TYPE[type] = Decoder;
  }

  /**
   * Registers the given chunk type as not automatically decodable. This causes
   * the decode() instance method to just return the RawChunk. Types should be
   * registered with this if they have dependencies and require further proccessing
   * to decode.
   * @param {ChunkType|string} type The type of chunk to exclude from decoding.
   */
  static exclude(type) {
    RawChunk.NOT_DECODABLE.push(type);
  }

  /**
   * Decodes a chunk header from the given buffer.
   * @param {ArrayBuffer} bytes The buffer that contains the chunk.
   * @param {number} pointer A pointer to the start of the chunk.
   * @return {RawChunk} The chunk of raw data.
   */
  static fromBytes(bytes, pointer) {
    const header = new DataView(bytes, pointer, 8);
    const length = header.getUint32(0);
    const type = String.fromCharCode(header.getUint8(4), header.getUint8(5),  header.getUint8(6), header.getUint8(7));
    const dataPointer = pointer + header.byteLength;
    const data = bytes.slice(dataPointer, dataPointer + length);
    const checksum = new DataView(bytes, pointer, header.byteLength + length + 4).getUint32(header.byteLength + length);

    return new RawChunk(type, checksum, data);
  }

  constructor(type, checksum, bytes) {
    super(type);

    this.checksum = checksum;
    this.bytes = bytes;
  }

  /**
   * @property {number} length
   * The length of the chunk data in bytes.
   */
  get length() {
    return this.bytes.byteLength;
  }

  /**
   * Attempts to decode the chunk and extract its data into a chunk class
   * instance. If the chunk cannot be decoded then this instance will be
   * returned. If the chunk type is not recognised then a warning will be
   * shown, if it is also marked as essential then an exception is thrown.
   * @throws {Error} Thrown when chunk is unknown but critical.
   * @return {Chunk} An instance of the relevant Chunk subclass.
   */
  async decode() {
    const decoder = RawChunk.CHUNK_CLASS_BY_TYPE[this.type];

    if (RawChunk.NOT_DECODABLE.includes(this.type)) {
      return this;
    }

    if (decoder) {
      return await decoder.decode(this);
    }

    if (this.isOptional) {
      console.warn(`PNG Chunk type ${this.type} not recognised.`);
      return this;
    }
    
    throw new Error(`PNG Chunk type ${this.type} not recognised.`);
  }

  /**
   * Validates that this chunks data has not been corrupted by computing
   * its checksum and comparing that with the stored copy. If the checksum
   * does not match the stored copy then an exception is thrown.
   * @throws {Error} Thrown when the checksums do not match.
   */
  validate() {
    const checksum = crc32(this.bytes);

    // FIXME -- Checksum must include the type bytes at the start, not just
    //          the chunk data. Also need to calculate a Uint32 number but
    //          the checksum code generates Int32 numbers in Javascrcipt.

    if (this.checksum !== checksum) {
      throw new Error(`${this.type} checksum does not match (expected "${this.checksum}" but got "${checksum}")`);
    }
  }
}