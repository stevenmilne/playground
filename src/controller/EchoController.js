const { AbstractController, Schema, SchemaType } = require('@smilne/socket-router');
const { Logger } = require('@smilne/util');

class EchoController extends AbstractController {
	constructor(Adapter) {
		super();

		this.adapter = new Adapter(this.schema());
	}

	async onMessage(connection, message) {
		const data = await this.adapter.deserialise(message);

		Logger.getLogger().log(`  message: ${data.message}`);
		if (data.message === 'schema') {
			return this.adapter.getSchema();
		}

		return this.adapter.serialise({
			message: data.message
		});
	}

	schema() {
		return new Schema()
			.property('message', SchemaType.STRING, true);
	}
}

module.exports = EchoController;
