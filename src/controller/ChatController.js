const { AbstractController, Schema, SchemaType, validate } = require('@smilne/socket-router');
const { Logger } = require('@smilne/util');

const Actions = {
	SCHEMA: 'schema',
	REGISTER: 'register',
	SEND: 'send',
	BROADCAST: 'broadcast',
	LIST: 'list'
};

class ChatController extends AbstractController {
	constructor(Adapter) {
		super();

		this.in = new Adapter(this.inSchema());
		this.out = new Adapter(this.messageSchema());
	}

	async onMessage(connection, message) {
		const data = await this.in.deserialise(message);

		Logger.getLogger().log(`  action: ${data.action}`);
		switch (data.action) {
			case Actions.SCHEMA:
				connection.send(this.in.getSchema());
				connection.send(this.out.getSchema());
				break;
			case Actions.REGISTER:
				Logger.getLogger().log(`  username: ${data.username}`);
				connection.username = data.username;
				break;
			case Actions.BROADCAST:
				Logger.getLogger().log(`  message: ${data.message}`);
				await this.broadcast(connection.username, data.message);
				break;
			case Actions.SEND:
				Logger.getLogger().log(`  to: ${data.to}`);
				Logger.getLogger().log(`  from: ${connection.username}`);
				Logger.getLogger().log(`  message: ${data.message}`);
				await this.send(connection.username, data.to, data.message);
				break;
			case Actions.LIST:
				Logger.getLogger().log('  listing all registered users');
				await this.list(connection);
				break;
		}
	}

	async broadcast(from, message) {
		if (!from) return;

		for (const connection of this.connections) {
			const payload = await this.out.serialise({ from, message });
			connection.send(payload);
		}
	}

	async send(from, to, message) {
		if (!from || !to) return;

		const payload = await this.out.serialise({ from, message });
		const connection = this.findUser(to);
		if (connection) {
			connection.send(payload);
		}
	}

	async list(client) {
		const users = [];

		for (const connection of this.connections) {
			if (connection.username !== client.username) {
				users.push(connection.username);
			}
		}

		// TODO -- Serialise the payload
	}

	findUser(username) {
		for (const connection of this.connections) {
			if (connection.username === username) {
				return connection;
			}
		}
	}

	inSchema() {
		return new Schema()
			.valueSet('action', SchemaType.STRING, [
				Actions.SCHEMA,
				Actions.REGISTER,
				Actions.SEND,
				Actions.BROADCAST,
				Actions.LIST
			])
			.property('action', 'action', true)
			.property('username', SchemaType.STRING, validate.when('action', Actions.REGISTER))
			.property('to', SchemaType.STRING, validate.when('action', Actions.SEND))
			.property('message', SchemaType.STRING, validate.when('action', [Actions.SEND, Actions.BROADCAST]));
	}

	messageSchema() {
		return new Schema()
			.property('message', SchemaType.STRING, true)
			.property('from', SchemaType.STRING, true);
	}

	listSchema() {
		return new Schema()
			.array('users', SchemaType.STRING)
			.property('users', 'users', true);
	}
}

module.exports = ChatController;
