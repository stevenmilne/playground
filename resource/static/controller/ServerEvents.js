const { xhr } = await require('/module/util.js');

const server = new EventSource('/api/events');

server.addEventListener('message', event => {
  console.log(event);
});

await xhr('/api/events', 'POST', 'Hello world!');
