const { Middleware } = require('@smilne/router');
const { HttpMethod, headersToString, contentType } = require('@smilne/http-utils');

module.exports = class HttpTrace extends Middleware.AbstractMiddleware {

	/**
	 * @override
	 * Intercepts all requests and handles them if the HTTP method is TRACE.
	 * @param {Request} request The request that is being handled.
	 * @return {boolean|undefined} Return false to mark the request as handled.
	 */
	async before(request) {
		if (request.getMethod() === HttpMethod.TRACE) {
			const response = await this.serialise(request);

			request.respond(response, 200, { ...contentType('message/http') });

			return false;
		}
	}

	/**
	 * @private
	 * Converts the given request object back to the text that it represents.
	 * @param {Request} request The request to serialise.
	 * @return {string} The stringified request.
	 */
	async serialise(request) {
		let text = `${request.getMethod()} ${request.getPathname()} HTTP/1.1\n`;

		text += headersToString( this.sanitiseHeaders(request) );
		text += '\n\n';
		text += await request.getBody();

		return text;
	}

	/**
	 * @private
	 * Returns filters headers from the given request. All HTTP headers whic may
	 * contain sensitive information will be removed.
	 * @param {Request} request The request whose headers are to be sanitised.
	 * @return {Object} An object with header values mapped by name.
	 */
	sanitiseHeaders(request) {
		const headers = Object.assign({}, request.getHeaders());

		delete headers.authorization;
		delete headers.cookie;
		delete headers.from;
		delete headers['proxy-authorization'];

		return headers;
	}
}
