
/**
 * Decompresses data which has been compressed with the ZLib Deflate
 * algorithm.
 * @param {ArrayBuffer} buffer The data to decompress.
 * @return {Promise<Uint8Array>} A promise which resolves with the data.
 */
exports.inflate = buffer => new Promise((resolve, reject) => {
  Zlib.inflate(new Uint8Array(buffer), (error, data) => {
    if (error) {
      reject(error);
      return;
    }

    resolve(data);
  })
});