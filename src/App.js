const { Router, ContentSecurityPolicy, SandboxException, Middleware } = require('@smilne/router');
const { cacheControl, HttpStatus, HttpStatusByCode } = require('@smilne/http-utils');
const MessageController = require('./controller/MessageController.js');
const LibraryController = require('./controller/LibraryController.js');
const StreamController = require('./controller/StreamController.js');
const EventsController = require('./controller/EventsController.js');
const RangeController = require('./controller/RangeController.js');
const EchoController = require('./controller/EchoController.js');
const ChatController = require('./controller/ChatController.js');
const HttpTrace = require('./server/middleware/HttpTrace.js');
const Minify = require('./server/middleware/Minify.js');
const Config = require('../config.js');
const { AbstractApp } = require('@smilne/server');
const { template } = require('./util/templateHelpers.js');

const DEFAULT_TEMPLATE_DATA = {
  header: template('header'),
  includes: template('includes')
};

module.exports = class App extends AbstractApp {

	configure() {
		return {
			hostname: Config.HOST_NAME,
			servername: Config.SERVER_NAME,
			httpPort: Config.HTTP_PORT,
			httpsPort: Config.HTTPS_PORT,
			templateDir: Config.TEMPLATE_DIR,
			templateExt: Config.TEMPLATE_EXT
		};
	}

	route(router) {
		router
			.install(new Middleware.WasmAssembler(Config.STATIC_DIR))
			.install(new HttpTrace())
      .install(new Minify(Config.STATIC_DIR))
			.delegate('/', this.getPageRouter(''))
			.delegate('/api', this.getApiRouter('/api'))
			.delegate('/redirect', this.getRedirectRouter('/redirect'))
			.delegate('/private', this.getPrivateRouter('/private'))
			.socket('/echo', EchoController, ['plaintext', 'json+schema', 'xml+schema'])
			.socket('/chat', ChatController, ['json+schema', 'xml+schema']);
	}

	getPageRouter(prefix) {
		return new Router({ prefix, exact: true })
			.withSecurityPolicy(
				new ContentSecurityPolicy()
					.setObjectSource(`https://${Config.HOST_NAME}`)
					.setUpgradeInsecure()
					.setSandbox([
						SandboxException.ALLOW_FORMS,
						SandboxException.ALLOW_SAME_ORIGIN,
						SandboxException.ALLOW_SCRIPTS
					])
			)
			.page('/', 'page/index', DEFAULT_TEMPLATE_DATA)
			.page('/rest', 'page/rest', DEFAULT_TEMPLATE_DATA)
			.page('/websockets', 'page/websockets', DEFAULT_TEMPLATE_DATA)
			.page('/webworker', 'page/webworker', DEFAULT_TEMPLATE_DATA)
			.page('/webassembly', 'page/webassembly', DEFAULT_TEMPLATE_DATA)
			.page('/streams', 'page/streams', DEFAULT_TEMPLATE_DATA)
      .page('/serverevents', 'page/serverevents', DEFAULT_TEMPLATE_DATA)
      .page('/messages', 'page/messages', DEFAULT_TEMPLATE_DATA)
      .page('/webgl', 'page/webgl', DEFAULT_TEMPLATE_DATA)
      .page('/testing', 'page/testing', DEFAULT_TEMPLATE_DATA)
			.on404(this.handle404);
	}

	getApiRouter(prefix) {
		return new Router({ prefix })
			.withSSLUpgrade(Config.HOST_NAME, Config.HTTPS_PORT)
			.withCORS({ origin: '*' })
			.withHeaders({ ...cacheControl('private') })
			.get('/hello', request => request.respondText('Hello world!'))
			.controller('/message', MessageController)
			.controller('/stream', StreamController)
			.controller('/logs', new RangeController(Config.LOG_FILE, 'text/plain'))
      .controller('/events', new EventsController())
      .controller('/library',  new LibraryController(`${Config.DATA_DIR}/library`));
	}

	getRedirectRouter(prefix) {
		return new Router({ prefix, exact: true })
			.redirect('/google', 'https://google.co.uk/');
	}

	getPrivateRouter(prefix) {
		return new Router({ prefix, exact: true })
			.withSSLUpgrade(Config.HOST_NAME, Config.HTTPS_PORT)
			.withAuthentication({ 'steven': 'password' }, 'Access to the good stuff.')
			.page('/test', 'test', { msg: 'And so does authentication! :D' });
	}

	handle404(request) {
		const url = request.url.pathname;
		const data = {
			code: HttpStatus.NOT_FOUND,
			status: HttpStatusByCode[HttpStatus.NOT_FOUND],
			message: `Resource "${url}" could not be found.`
		};

		request.render('error', data, HttpStatus.NOT_FOUND);
	}
};
