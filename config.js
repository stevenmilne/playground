
const path = resource => resource ? `${__dirname}/${resource}` : __dirname;

const configuration = {
	SERVER_NAME: 'NodeJS/10.6.0 (macOS)',
	DOMAIN_NAME: 'nodejs.smilne.dev',
	HOST_NAME: 'localhost',
	HTTP_PORT: 3000,
	HTTPS_PORT: 3443,
	ROOT_DIRECTORY: path(),
	TEMPLATE_DIR: path('resource/template'),
	TEMPLATE_EXT: 'html',
	STATIC_DIR: path('resource/static'),
	DATA_DIR: path('resource/data'),
	SSL_KEY: path('resource/data/ssl.key'),
	SSL_CERT: path('resource/data/ssl.cert'),
	LOG_FILE: path('log.txt')
};

module.exports = configuration;
