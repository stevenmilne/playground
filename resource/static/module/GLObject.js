
module.exports = class GLObject {

  /**
   * Constructor for a generic GLObject which can be drawn in a WebGL context on
   * a web page.
   * @param {WebGLRenderingContext} gl The WebGL context.
   * @param {Object} shaderData Pointers to the uniform and attribute variables.
   * @param {Object} data The arrays which make up the object.
   * @param {number[]} data.vertex The vertex co-oridinates.
   * @param {number[]} data.colour The colour values for each vertex.
   * @param {number[]} data.index Indices into the vertex buffer.
   */
  constructor(gl, shaderData, { vertex, colour, index }) {
    this.gl = gl
    this.vertexCount = vertex.length / 2

    this.buffers = {
      vertex: this.newBuffer(gl.ARRAY_BUFFER, gl.STATIC_DRAW, new Float32Array(vertex)),
      colour: this.newBuffer(gl.ARRAY_BUFFER, gl.STATIC_DRAW, new Float32Array(colour)),
      // index: this.newBuffer(gl.ELEMENT_ARRAY_BUFFER, gl.STATIC_DRAW, new Uint16Array(index))
    }

    this.attributes = {
      vertex: shaderData.attributes.vertexPosition,
      colour: shaderData.attributes.vertexColour
    }
  }

  /**
   * Renders the object into the current scene.
   */
  draw() {
    const { gl, buffers, attributes } = this

    this.uploadBuffer(gl.ARRAY_BUFFER, buffers.vertex, 2, gl.FLOAT, attributes.vertex)
    this.uploadBuffer(gl.ARRAY_BUFFER, buffers.colour, 4, gl.FLOAT, attributes.colour)

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.vertexCount)
  }

  /**
   * @private
   * Creates a new buffer on the GPU to hold object data.
   * @param {GLenum} type The type of buffer to allocate.
   * @param {GLenum} usage The intended purpose of the buffer.
   * @param {ArrayBuffer} value Data to upload to  the buffer.
   * @return {WebGLBuffer} A reference to the new buffer.
   */
  newBuffer(type, usage, value) {
    const gl = this.gl
    const buffer = gl.createBuffer()

    gl.bindBuffer(type, buffer)
    gl.bufferData(type, value, usage)

    return buffer
  }

  /**
   * @private
   * Uploads new data to the provided GPU buffer and readies it to be used by
   * the shader.
   * @param {GLenum} bufferType The type of the buffer being uploaded to.
   * @param {WebGLBuffer} buffer The buffer to ready.
   * @param {number} components The number of elements to be used for each vertex.
   * @param {GLenum} type The type of data being uploaded.
   * @param {GLint} attribute The pointer to the attribute variable to bind the buffer to.
   */
  uploadBuffer(bufferType, buffer, components, type, attribute) {
    const gl = this.gl

    gl.bindBuffer(bufferType, buffer)
    gl.vertexAttribPointer(attribute, components, type, false, 0, 0)
    gl.enableVertexAttribArray(attribute)
  }
}
