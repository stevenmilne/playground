
const table = [];

function setupTable() {
  let value = 0;

  for(let i = 0; i < 256; i++) {
    value = i;

    for(let j = 0; j < 8; j++) {
      if (value & 1) {
        value = 0xedb88320 ^ (value >> 1);
      }  else {
        value = value >> 1;
      }

      table[i] = value;
    }
  }
}

/**
 * @private
 * @param {number} crc 
 * @param {Uint8Array} bytes 
 * @return {number}
 */
function updateCrc(crc, bytes) {
  let value = crc;

  for(let i = 0; i < bytes.length; i++) {
    value = table[(value ^ bytes[i]) & 0xff] ^ (value >> 8);
  }

  return value;
}

/**
 * Computes a CRC32 checksum for the given array buffer.
 * @param {ArrayBuffer} buffer The data to checksum.
 * @return {number}
 */
function crc32(buffer) {
  const bytes = new Uint8Array(buffer);
  
  return updateCrc(0xffffffff, bytes) ^ 0xffffffff;
}

setupTable();

module.exports = {
  crc32
};