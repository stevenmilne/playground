const { readFields, NumberReader } = await require('/module/png/FieldReader.js');
const { Chunk, ChunkType } = await require('/module/png/chunk/Chunk.js');
const RawChunk = await require('/module/png/chunk/RawChunk.js');

class AspectRatioChunk extends Chunk {

  static decode(chunk) {
    chunk.assert(ChunkType.pHYs);

    const [ x, y, units ] = readFields(chunk.bytes, [
      new NumberReader(4),
      new NumberReader(4),
      new NumberReader()
    ]);

    return new AspectRatioChunk(chunk.type, { x, y, units });
  }

  constructor(type, { x, y, units }) {
    super(type);

    this.x = x;
    this.y = y;
    this.units = units;
  }
}

RawChunk.register(ChunkType.pHYs, AspectRatioChunk);
module.exports = AspectRatioChunk;