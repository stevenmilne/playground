const { readFields, TextReader, NumberReader, DataReader } = await require('/module/png/FieldReader.js');
const { Chunk, ChunkType } = await require('/module/png/chunk/Chunk.js');
const RawChunk = await require('/module/png/chunk/RawChunk.js');
const { inflate } = await require('/module/png/zlib.js');

class InternationalTextChunk extends Chunk {

  static async decode(chunk) {
    chunk.assert(ChunkType.iTXt);

    const [ key, compressed, method, language, ikey, raw ] = readFields(chunk.bytes, [
      new TextReader(79),
      new NumberReader(),
      new NumberReader(),
      new TextReader(),
      new TextReader(),
      new DataReader()
    ]);
    
    if (compressed && method === 0) {
      const compressedData = await inflate(raw);
      const value = new TextDecoder().decode(compressedData);

      return new InternationalTextChunk(chunk.type, { key, language, ikey, value });
    }

    const value = new TextDecoder().decode(raw);
    return new InternationalTextChunk(chunk.type, { key, language, ikey, value });
  }

  constructor(type, { key, language, ikey, value }) {
    super(type);

    this.key = key;
    this.ikey = ikey;
    this.value = value;
    this.language = language;
  }

  /**
   * @override
   * This function is implemented for typed chunks and returns a flag
   * which specifies if multiple chunks in the same file are supported.
   * @return {boolean} True if this chunk type supports duplicate chunks.
   */
  isStackable() {
    return true;
  }

  /**
   * @override
   * This function is implemented for stackable typed chunks. Chunks which
   * support being mapped by a keyword can implement this function to return
   * the unique keyword that can identify a chunk.
   * @return {string} A value that can map this chunk instance.
   */
  getKey() {
    return this.key;
  }
}

RawChunk.register(ChunkType.iTXt, InternationalTextChunk);
module.exports = InternationalTextChunk;