
class ChatController {
	constructor(root) {
		this.socket = new WebSocket('ws://localhost:3000/chat', 'json+schema');
		this.root = root;

		this.cacheElements(root);
		this.registerListeners();
	}

	cacheElements(root) {
		this.elements = {
			registration: root.querySelector('#registerSection'),
			register: root.querySelector('#register'),
			send: root.querySelector('#send'),
			username: root.querySelector('#username'),
			usernameDisplay: root.querySelector('#usernameDisplay'),
			message: root.querySelector('#message'),
			output: root.querySelector('#output')
		};
	}

	registerListeners() {
		this.elements.register.addEventListener('click', this.handleRegister.bind(this));
		this.elements.send.addEventListener('click', this.handleSend.bind(this));

		this.socket.onmessage = this.handleMessage.bind(this);
	}

	handleRegister() {
		const username = this.elements.username.value;

		this.socket.send(JSON.stringify({
			action: 'register',
			username
		}));

		this.elements.usernameDisplay.innerText = `Username: ${username}`;
		this.show(this.elements.usernameDisplay);
		this.hide(this.elements.registration);
	}

	handleSend() {
		const message = this.elements.message.value;

		this.socket.send(JSON.stringify({
			action: 'broadcast',
			message
		}));

		this.elements.message.value = '';
	}

	handleMessage(event) {
		const data = JSON.parse(event.data);
		const output = this.elements.output;

		output.innerText += `${data.from}: ${data.message}\n`;
		console.log(`Message from "${data.from}": ${data.message}`);
	}

	hide(element) {
		element.classList.add('hidden');
	}

	show(element) {
		element.classList.remove('hidden');
	}
}

module.exports = ChatController;
