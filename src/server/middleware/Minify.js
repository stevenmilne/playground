const UglifyJS = require('uglify-js');
const { Middleware } = require('@smilne/router');
const { FileSystem, Logger } = require('@smilne/util');
const { HttpStatus, HttpMethod, contentType } = require('@smilne/http-utils');

module.exports = class Minify extends Middleware.AbstractMiddleware {

  constructor(prefix) {
		super();

		this.prefix = prefix;
	}

	/**
	 * @override
	 * Intercepts all requests and handles them if the HTTP method is TRACE.
	 * @param {Request} request The request that is being handled.
	 * @return {boolean|undefined} Return false to mark the request as handled.
	 */
	async before(request) {
    const logger = Logger.getLogger();
		if (request.getMethod() !== HttpMethod.GET) return;
    if (request.getPathname() && !request.getPathname().match(/\.min\.js$/)) return;

    const target = this.prefix + request.getPathname();
    const source = target.replace('.min.js', '.js');

    try {
      await FileSystem.exists(source);
      const buffer = await FileSystem.read(source);
      const minified = UglifyJS.minify(buffer.toString());

      request.respond(minified, HttpStatus.OK, { ...contentType('text/javascript') });
    } catch (error) {
      logger.error(`  Failed to minify file: ${error.stack || error}`);
			throw error;
    }

    return false;
	}

}
