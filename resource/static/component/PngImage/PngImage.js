(async function () {
  const PNG = await require('/module/PNG.js');

  class PngImage extends HTMLElement {

    static ATTRIBUTES = {
      SRC: 'src'
    };

    static get observedAttributes() {
      return Object.values(PngImage.ATTRIBUTES);
    }

    constructor() {
      super();

      this.render();
      this.png = null;
    }

    attributeChangedCallback(name, oldValue, value) {
      switch(name) {
        case PngImage.ATTRIBUTES.SRC:
          return this.handleSrcChange(value);
      }
    }

    /**
     * @private
     * @param {string} value The new src value.
     */
    async handleSrcChange(value) {
      if (!value) {
        this.clear();
        this.png = null;
        return;
      }

      const blob = await load(value);
      this.png = await PNG.fromBlob(blob, { noChecksum: true });

      this.draw(this.png);
    }

    /**
     * @private
     * Sets up the shadow DOM and renders the components markup into it.
     */
    render() {
      const root = this.attachShadow({ mode: 'closed' });
      const canvas = document.createElement('canvas');

      root.appendChild(canvas);

      this.canvas = canvas;
    }

    /**
     * @private
     * Renders the given decoded PNG data into the canvas element. 
     * @param {PngFile} png The PNG data to render.
     */
    draw(png) {
      this.canvas.height = png.height;
      this.canvas.width = png.width;

      const context = this.canvas.getContext('2d');
      context.putImageData(png.image, 0, 0);
    }

    /**
     * @private
     * Clears the canvas element to a blank state.
     */
    clear() {
      const context = this.canvas.getContext('2d');
      const { width, height } = this.canvas;

      context.clearRect(0, 0, width, height);
    }
  }

  /**
   * @private
   * Load a PNG image from the specified URL.
   * @param {string} url The URL of the PNG to load and display
   * @return {Blob} The loaded image data.
   */
  async function load(url) {
    const response = await fetch(url);

    return await response.blob();
  }

  customElements.define('png-image', PngImage);
})();