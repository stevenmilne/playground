const { readFields, NumberReader } = await require('/module/png/FieldReader.js');
const { Chunk, ChunkType } = await require('/module/png/chunk/Chunk.js');
const RawChunk = await require('/module/png/chunk/RawChunk.js');

class HeaderChunk extends Chunk {

  /**
   * @static
   * Decodes a raw chunk into a new instance of a header chunk.
   * @param {RawChunk} chunk The raw chunk to decode.
   * @return {HeaderChunk} The decoded header chunk.
   */
  static decode(chunk) {
    chunk.assert(ChunkType.IHDR);

    const [ width, height, depth, mode, compression, filter, interlace  ] = readFields(chunk.bytes, [
      new NumberReader(4),
      new NumberReader(4),
      new NumberReader(),
      new NumberReader(),
      new NumberReader(),
      new NumberReader(),
      new NumberReader()
    ]);

    return new HeaderChunk(chunk.type, { width, height, depth, mode, compression, filter, interlace });
  }

  constructor(type, { width, height, depth, mode, compression, filter, interlace }) {
    super(type);

    this.width = width;
    this.height = height;
    this.depth = depth;
    this.mode = mode;
    this.compression =  compression;
    this.filter = filter;
    this.interlace = interlace;
  }

  /**
   * Calculates the number of bytes each pixel uses.
   * @return {number} The number of bytes per pixel.
   */
  getPixelBytes() {
    switch(this.mode) {
      case 0: // Greyscale
        return this.depth;
      case 2: // Colour used
        return (this.depth * 3) / 8;
      case 3: // Colour and palette used
        return this.depth / 8;
      case 4: // Alpha channel used
        return (this.depth * 2) / 8;
      case 6: // Colour and alpha used
        return (this.depth * 4) / 8;
      default:
        throw new Error(`PNG colour mode "${this.mode}" not recognised.`);
    }
  }
}

RawChunk.register(ChunkType.IHDR, HeaderChunk);
module.exports = HeaderChunk;