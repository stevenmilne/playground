const { toRadians } = await require('/module/util.js')
const GLObject = await require('/module/GLObject.js')

const vertexShader = await require('/shader/basic.vert')
const fragmentShader = await require('/shader/basic.frag')

const CONTEXT_WEBGL = 'webgl'

const gl = initWebGl()
const program = initShaders(vertexShader, fragmentShader)

const shaderData = {
  attributes: {
    vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
    vertexColour: gl.getAttribLocation(program, 'aVertexColour')
  },
  uniforms: {
    projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
    modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),
  }
}

const square = new GLObject(gl, shaderData, {
  vertex: [
    -1.0,  1.0,
     1.0,  1.0,
    -1.0, -1.0,
     1.0, -1.0
  ],
  colour: [
    1.0, 1.0, 1.0, 1.0,
    1.0, 0.0, 0.0, 1.0,
    0.0, 1.0, 0.0, 1.0,
    0.0, 0.0, 1.0, 1.0
  ]
})

requestAnimationFrame(onAnimationFrame)

function onAnimationFrame() {
  const { attributes, uniforms } = shaderData

  gl.clearColor(0.0, 0.0, 0.0, 1.0)
  gl.clearDepth(1.0)
  gl.enable(gl.DEPTH_TEST)
  gl.depthFunc(gl.LEQUAL)

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

  const projectionMatrix = setupCamera(45, gl.canvas.clientWidth / gl.canvas.clientHeight,  0.1, 100.0)

  const viewMatrix = mat4.create();
  mat4.translate(viewMatrix, viewMatrix, [-0.0, 0.0, -6.0])

  gl.useProgram(program)

  gl.uniformMatrix4fv(uniforms.projectionMatrix, false, projectionMatrix)
  gl.uniformMatrix4fv(uniforms.modelViewMatrix, false, viewMatrix)

  square.draw()
  // triangle.draw()

  requestAnimationFrame(onAnimationFrame)
}

function initWebGl() {
  const canvas = document.querySelector('#canvas')
  canvas.height = canvas.clientHeight
  canvas.width = canvas.clientWidth

  const gl = canvas.getContext(CONTEXT_WEBGL)

  gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight)
  gl.clearColor(0.0, 0.0, 0.0, 1.0)
  gl.clear(gl.COLOR_BUFFER_BIT)

  return gl
}

function initShader(type, source) {
  const shader = gl.createShader(type)

  gl.shaderSource(shader, source)
  gl.compileShader(shader)

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    const error = gl.getShaderInfoLog(shader)
    alert(`Failed to compile shader: ${error}`)
    return null
  }

  return shader
}

function initShaders(vertexSrc, fragmentSrc) {
  const program =  gl.createProgram()

  gl.attachShader(program, initShader(gl.VERTEX_SHADER, vertexSrc))
  gl.attachShader(program, initShader(gl.FRAGMENT_SHADER, fragmentSrc))
  gl.linkProgram(program)

  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    const error = gl.getProgramInfoLog(program)
    alert(`Failed to link shaders: ${program}`)
    return null
  }

  return program
}

function setupCamera(fov, aspectRatio, zNear, zFar) {
  const projectionMatrix = mat4.create();

  mat4.perspective(projectionMatrix, toRadians(fov), aspectRatio, zNear, zFar)

  return projectionMatrix
}
