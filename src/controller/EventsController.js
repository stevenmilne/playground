const { EventController } = require('@smilne/router');
const { HttpStatus } = require('@smilne/http-utils');

module.exports = class EventsController extends EventController {
  async post(request) {
    this.fire('message', await request.getBody());

    request.respond(null, HttpStatus.NO_CONTENT);
  }
}
