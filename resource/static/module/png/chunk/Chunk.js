const { isLowerCase } = await require('/module/util');

const ChunkType = {
  IHDR: 'IHDR',
  PLTE: 'PLTE',
  IDAT: 'IDAT',
  IEND: 'IEND',
  cHRM: 'cHRM',
  gAMA: 'gAMA',
  iCCP: 'iCCP',
  sBIT: 'sBIT',
  sRGB: 'sRGB',
  bKGD: 'bKGD',
  hIST: 'hIST',
  tRNS: 'tRNS',
  pHYs: 'pHYs',
  sPLT: 'sPLT',
  tIME: 'tIME',
  iTXt: 'iTXt',
  tEXt: 'tEXt',
  zTXt: 'zTXt'
};

class Chunk {

  /**
   * @property {ChunkType|string} type
   * Designates the type of the chunk.
   */

  /**
   * Constructs a new generic chunk with the given type.
   * @param {ChunkType} type The type of the chunk.
   */
  constructor(type) {
    this.type = type;
  }

  /**
   * @property {boolean} isOptional
   * True if the chunk type is optional. This is indicated by the first
   * character of the type being lowercase.
   */
  get isOptional() {
    return isLowerCase(this.type[0]);
  }

  /**
   * @property {boolean} isPrivate
   * True if the chunk type is private and not part of the PNG specification
   * or included in the public extensions registry.
   */
  get isPrivate() {
    return isLowerCase(this.type[1]);
  }

  /**
   * @property {boolean} isCopyable
   * True if the chunk type can be copied/written to a modified
   * file if it is unknown. If false then this chunk must be discarded
   * if the decoder does not understand it and the file has been
   * modified.
   */
  get isCopyable() {
    return isLowerCase(this.type[3]);
  }

  /**
   * Asserts that the chunk is the given type, if it is not then an
   * exception is thrown.
   * @param {ChunkType} type The that this chunk is expected to be.
   * @throws {Error} Thrown when chunk is not asserted type.
   */
  assert(type) {
    if (type !== this.type) {
      throw new Error(`Expected chunk type ${this.type} but got ${type}`);
    }
  }

  /**
   * @abstract
   * This function is implemented for typed chunks and returns a flag
   * which specifies if multiple chunks in the same file are supported.
   * @return {boolean} True if this chunk type supports duplicate chunks.
   */
  isStackable() {
    return false;
  }

  /**
   * @abstract
   * @function getKey
   * This function is implemented for stackable typed chunks. Chunks which
   * support being mapped by a keyword can implement this function to return
   * the unique keyword that can identify a chunk.
   * @return {string} A value that can map this chunk instance.
   */
}

module.exports = { Chunk, ChunkType };