
/**
 * Generates a new rand UUID.
 * @return {string} A ransomised UUID.
 */
exports.uuid = () =>
	'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, character => {
			const now = new Date().getTime();
			const random = (now + Math.random() * 16) % 16 | 0;
			return (character == 'x' ? random : (random & 0x3 | 0x8)).toString(16);
	});

/**
 * Converts a collection of bytes from WebAssembly memory into a Javascript
 * string.
 * @param {WebAssembly.Memory} memory The memory that contains the text bytes.
 * @param {number} pointer A pointer to the start of the string in memory.
 * @param {number} length The length of the string.
 * @return {string} The converted string.
 */
exports.bytes2string = (memory, pointer, length) =>
	new TextDecoder().decode(
		new Uint8Array(memory.buffer, pointer, length)
	)

/**
 * Writes a Javascript string into WebAssembly memory.
 * @param {WebAssembly.Memory} memory The memory to inject the string into.
 * @param {number} pointer A pointer to the address where the string should start.
 * @param {string} value The string to convert.
 */
exports.string2bytes = (memory, pointer, value) =>
	new Uint8Array(memory.buffer, pointer, value.length)
		.set(new TextEncoder().encode(value))

/**
 * Invokes a promise wrapped XMLHttpRequest for the specified resource.
 * @param {string} url The URL of the resource to request.
 * @param {string} [method='GET'] The HTTP method to use for the request.
 * @param {Stream|Blob|string} body The data to be sent with the request.
 * @return {Promise<string>} A promise that resolves to the response text.
 */
exports.xhr = (url, method = 'GET', body) => new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();

    request.addEventListener('load', () => resolve(request.responseText));
    request.addEventListener('error', (error) => reject(error));
    request.open(method, url);
    request.send(body);
  });

/**
 * Converts the given number of degrees to radians.
 * @param {number} degrees The degrees to convert.
 * @return {number} The converted radians.
 */
exports.toRadians = degrees => degrees * Math.PI / 180

/**
 * Converts an array of decimal numbers into a hexadecimal string.
 * @param {number[]} array The array of numbers to convert
 * @return {string} The converted hexadecimal string.
 */
exports.toHex = array => array
	.map(item => item.toString(16))
	.map(item => item.length < 2 ? `0${item}` : item)
	.join(' ');

/**
 * Checks if a character is lower case.
 * @param {string} char A single character to check.
 * @return {boolean} True if the character is lower case.
 */
exports.isLowerCase = char => char.toLowerCase() === char