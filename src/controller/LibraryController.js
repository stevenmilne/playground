const { HttpStatus, contentType } = require('@smilne/http-utils');
const { AbstractController } = require('@smilne/router');
const { FileSystem, Logger } = require('@smilne/util');
const Config = require('../../config.js');

module.exports = class LibraryController extends AbstractController {
  constructor(dir) {
    super();

    this.library = dir;
  }

  async get(request) {
    const [ name, version ] = request.getUnmatched().split('/');

    if (!name) {
      request.respondText('Library name must be specified', HttpStatus.BAD_REQUEST);
      Logger.getLogger().warn('  Library name not specified');
      return;
    }

    if (version) {
      await this.getVersion(request, name, version);
    } else {
      await this.getMetadata(request, name);
    }
  }

  async put(request) {
    const [ name, version ] = request.getUnmatched().split('/');

    if (!name) {
      request.respondText('Library name must be specified', HttpStatus.BAD_REQUEST);
      Logger.getLogger().warn('  Library name not specified');
      return;
    }

    if (version) {
      await this.putVersion(request, name, version);
    } else {
      await this.putMetadata(request, name);
    }
  }

  async getMetadata(request, name) {
    const filename = `${Config.DATA_DIR}/library/${name}.meta.ini`;

    try {
      const content = await FileSystem.read(filename);

      request.respondText(content);
    } catch(error) {
      request.respondText('Library not found', HttpStatus.NOT_FOUND);
    }
  }

  async getVersion(request, name, version) {
    const filename = `${Config.DATA_DIR}/library/${name}-${version}.tar.gz`;

    try {
      const content = await FileSystem.read(filename);

      request.respond(content, HttpStatus.OK, { ...contentType('application/tar+gzip') });
    } catch(error) {
      request.respondText('Library not found', HttpStatus.NOT_FOUND);
    }
  }

  async putMetadata(request, name) {
    const filename = `${Config.DATA_DIR}/library/${name}.meta.ini`;

    try {
      FileSystem.write(filename, await request.getBody());

      request.respond(null, HttpStatus.NO_CONTENT);
    } catch(error) {
      request.respondText('Could not upload metadata', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async putVersion(request, name, version) {
    const filename = `${Config.DATA_DIR}/library/${name}-${version}.tar.gz`;

    try {
      FileSystem.write(filename, await request.getBuffer());

      request.respond(null, HttpStatus.NO_CONTENT);
    } catch(error) {
      request.respondText('Could not upload version binary', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
