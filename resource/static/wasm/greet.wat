(module
	(import "js" "log" (func $log (param i32 i32)))
  (import "js" "mem" (memory 1))
	(data (i32.const 500) "Hello ")					;; length = 6
	(data (i32.const 506) ", from WASM!")		;; length = 12

	(func $greet (export "greet") (param $ptr i32) (param $len i32)
		(call $memcpy					;; Copy the start of the greeting
			(i32.const 0)				;; target pointer
			(i32.const 500)			;; source pointer
			(i32.const 6)				;; length
		)

		(call $memcpy					;; Copy the name
			(i32.const 6)				;; target pointer
			(local.get $ptr)		;; source pointer
			(local.get $len)		;; length
		)

		(call $memcpy					;; Copy the end of the greeting
			(i32.add						;; Compute the start address of the continued greeting
				(local.get $len)
				(i32.const 6)
			)
			(i32.const 506)			;; source pointer
			(i32.const 12)			;; length
		)

		(call $log						;; Print joined strings
			(i32.const 0)				;; Pointer
			(i32.add						;; Computed length
				(local.get $len)
				(i32.const 18)
			)
		)

		;; Clean up joined strings
		(call $zeromem (i32.const 0) (i32.const 100))
	)

	;; Copies a block of memory from one location to another location in memory.
	;; Takes a target pointer to copy data to, a source pointer and a length to
	;; copy.
	(func $memcpy (param $tgt i32) (param $src i32) (param $len i32)
		(local $char i32)
		(local $i i32)

		i32.const 0
		local.set $i

		(loop
			(br_if 1 (i32.eq (local.get $i) (local.get $len)))	;; Break from loop if at length

			(i32.add (local.get $src) (local.get $i))	;; Compute the next character address
			i32.load8_u
			local.set $char

			(i32.add (local.get $tgt) (local.get $i))	;; Compute store address
			local.get $char
			i32.store8

			(call $inc (local.get $i))
			local.set $i
			br 0
		)
	)

	;; Zeros memory between the two given addresses inclusively.
	(func $zeromem (param $start i32) (param $end i32)
		(local $i i32)

		local.get $start
		local.set $i

		(loop
			(br_if 1 (i32.eq (local.get $i) (local.get $end)))

			(i32.store (local.get $i) (i32.const 0))

			(call $inc (local.get $i))
			local.set $i
			br 0
		)
	)

	;; Increments the provided value by 1.
	(func $inc (param $val i32) (result i32)
		local.get $val
		i32.const 1
		i32.add
	)

	;; Decrements the provided value by 1.
	(func $dec (param $val i32) (result i32)
		local.get $val
		i32.const 1
		i32.sub
	)
)
