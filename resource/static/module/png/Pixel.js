
const DEFAULT_ALPHA = 255;

module.exports = class Pixel {

  /**
   * Decodes a single pixel from PNG image data and returns a new pixel instance.
   * @param {number} mode The colour mode of the PNG image data.
   * @param {number} depth The number of bits per colour channel.
   * @param {DataView} bytes A view of the array buffer which the pixel is in.
   * @param {number} pointer A pointer to the start of the pixel.
   * @return {Pixel} The parsed pixel data.
   */
  static decode(mode, depth, bytes, pointer) {
    if (mode === 0) {
      const value = bytes.getUint8(pointer);

      return new Pixel(value, value, value, DEFAULT_ALPHA);
    }

    if (mode === 4 || mode === 6) {
      return new Pixel(
        bytes.getUint8(pointer),
        bytes.getUint8(pointer + 1),
        bytes.getUint8(pointer + 2),
        bytes.getUint8(pointer + 3)
      );
    }
  }
 
  constructor(red, green, blue, alpha) {
    this.red = red;
    this.green = green;
    this.blue = blue;
    this.alpha = alpha;
  }
}