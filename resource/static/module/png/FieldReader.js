/**
 * Example:
 *  const fields = readFields(chunk.bytes, [
 *    new TextReader(79),           // Key
 *    new NumberReader(),           // Compressed
 *    new NumberReader(),           // Compression method
 *    new TextReader(),             // Language
 *    new TextReader(),             // Translated Key
 *    new DataReader()              // Payload
 *  ]);
 */

 /**
 * Reads the specified fields from the given ArrayBuffer.
 * @param {ArrayBuffer} buffer The array buffer to read fields from.
 * @param {FieldReader[]} fields The types of fields to read.
 * @return {Array} An array of the field values read.
 */
function readFields(buffer, fields) {
  let pointer = 0;

  const values = [];
  while (pointer <= buffer.byteLength && values.length < fields.length) {
    const field = fields[values.length];

    values.push(
      field.read(buffer, pointer)
    );

    pointer += field.length || 1;
  }

  return values;
}

class FieldReader {}

class TextReader extends FieldReader {
  constructor(count) {
    super();

    this.count = count;
  }

  read(buffer, pointer) {
    const readTo = this.count ? Math.min(this.count, buffer.byteLength - pointer) : buffer.byteLength - pointer;
    const bytes = new Uint8Array(buffer, pointer, readTo);

    const string = this.tillNull(bytes);

    // Add one to the length if text was null terminated to consume the null byte
    // as part of this field.
    this.length = string.length === bytes.length ? string.length : string.length + 1;

    return new TextDecoder().decode(string);
  }

  tillNull(array) {
    for (let i = 0; i < array.length; i++) {
      if (array[i] === 0) {
        return array.slice(0, i);
      }
    }

    return array;
  }
}

class NumberReader extends FieldReader {
  constructor(length = 1) {
    super();

    this.length = length;
  }

  read(buffer, pointer) {
    const view = new DataView(buffer, pointer, this.length);
    
    switch(this.length) {
      case 1:
        return view.getUint8(0);
      case 2:
        return view.getUint16(0);
      case 4:
        return view.getUint32(0);
      default:
          throw new Error(`Cannot read ${this.length} byte number`);
    }
  }
}

class DataReader extends FieldReader {
  constructor() {
    super();
  }

  read(buffer, pointer) {
    const data = buffer.slice(pointer);

    this.length = data.length;

    return data;
  }
}

module.exports = {
  readFields,
  TextReader,
  NumberReader,
  DataReader
};