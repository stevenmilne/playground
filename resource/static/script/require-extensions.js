
const EXTENSIONS_PLAIN_TEXT = ['.txt', '.vert', '.frag'];

require.handle('.wasm', async (url, register) => {
	const response = await fetch(url);
	const bytes = await response.arrayBuffer();
	const module =  await WebAssembly.compile(bytes);

	return register(module);
});

require.handleAll(EXTENSIONS_PLAIN_TEXT, async (url, register) => {
  const response = await fetch(url);
  const content = response.text()

  return register(content);
});

require.handle('.html', async (url, register) => {
	const response = await fetch(url);
	const html = await response.text();
	const fragment = document.createRange().createContextualFragment(html);

	const parts = url.split('/');

	const module = new require.Module({
		id: parts[parts.length - 1],
		filename: url,
		paths: [url],
		exports: fragment
	});

	return register(module);
});

class RequireComponent extends HTMLElement {

	static ATTRIBUTES = {
		NAME: 'name'
	};

	static get observedAttributes() {
		return Object.values(RequireComponent.ATTRIBUTES);
	}

	constructor() {
		super();
	}

	attributeChangedCallback(name, oldValue, value) {
		switch(name) {
			case RequireComponent.ATTRIBUTES.NAME:
				this.require(value);
		}
	}

	async require(name) {
		if (!name) return;

		const url = `/component/${name}/${name}.html`;
		const fragment = await require(url);
		const el = document.createElement('div');

		el.id = `component-${name}`;
		el.dataset.url = url;

		const shadow = el.attachShadow({ mode: 'closed' });
		shadow.appendChild(
			fragment.cloneNode(true)
			);

		document.body.appendChild(el);
	}
}

customElements.define('require-component', RequireComponent);