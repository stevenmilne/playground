const { Chunk, ChunkType } = await require('/module/png/chunk/Chunk.js');
const RawChunk = await require('/module/png/chunk/RawChunk.js');
const { inflate } = await require('/module/png/zlib.js');
const Pixel = await require('/module/png/Pixel.js');

class ImageDataChunk extends Chunk {

  /**
   * Decodes a raw chunk into a new instance of an image data chunk.
   * @param {HeaderChunk} header The header chunk.
   * @param {RawChunk[]} chunks All of the image data chunks from the file.
   * @return {ImageDataChunk} The decoded image chunk.
   */
  static async decode(header, chunks) {
    chunks.map( chunk => chunk.assert(ChunkType.IDAT) );

    const compressed = joinChunks(chunks);
    const data = await inflate(compressed);

    const pixelLength = header.getPixelBytes();
    const length = 1 + header.width * pixelLength;
    const scanlines = [];

    let scanline = null;
    let pointer = 0;
    while(pointer < data.byteLength) {
      scanline = unfilter(pixelLength, data.buffer, pointer, length, scanline);

      const line = [];
      for (let i = 1; i < scanline.buffer.byteLength; i += pixelLength) {
        line.push(
          Pixel.decode(header.mode, header.depth, scanline, i)
        );
      }

      scanlines.push(line);
      pointer += length;
    }

    return new ImageDataChunk(ChunkType.IDAT, { scanlines });
  }

  constructor(type, { scanlines }) {
    super(type);

    this.scanlines = scanlines;
  }
}

/**
 * @private
 * Combines the buffers of all given chunks into the one buffer.
 * @param {RawChunk[]} chunks The chunks to be joined.
 * @return {ArrayBuffer} The joined chunk data.
 */
function joinChunks(chunks) {
  const buffers = chunks.map( chunk => chunk.bytes );
  const length = buffers.reduce((total, buffer) => total + buffer.byteLength, 0);
  const buffer = new ArrayBuffer(length);
  const bytes = new Uint8Array(buffer);

  let pointer = 0;
  buffers.map(buffer => {
    const data = new Uint8Array(buffer);
    bytes.set(data, pointer);
    pointer += buffer.byteLength;
  });

  return buffer;
}

/**
 * @private
 * Performs pre-compression scanline filtering to improve the compression
 * outcome.
 * @param {number} previous The byte value of the previous pixel.
 * @param {number} above The byte value of the pixel directly above.
 * @param {number} previousAbove The byte value of the pixel to the top left.
 * @return {number} The new filtered pixel byte value.
 */
const filter4 = (previous, above, previousAbove) => {
  const p = previous + above - previousAbove;
  const pa = Math.abs(p - previous);
  const pb = Math.abs(p - above);
  const pc = Math.abs(p - previousAbove);

  if (pa <= pb && pa <= pc) return previous;
  if (pb <= pc) return above;
  
  return previousAbove;
}

/**
 * @private
 * Removes the pre-compression filtering applied to scanline data. NOTE: The returned
 * scanline data is cloned so no changes are made to the original data.
 * @param {number} pixelLength The number of bytes each pixel uses.
 * @param {ArrayBuffer} buffer The whole IDAT chunk which contains the scanline.
 * @param {number} pointer A pointer to the start of the scanline.
 * @param {number} length The byte length of the scanline.
 * @param {DataView} previous The previous unfiltered scanline data.
 * @return {DataView} The unfiltered scanline data.
 */
function unfilter(pixelLength, buffer, pointer, length, previous) {
  const clone = buffer.slice(pointer, pointer + length);
  const view = new DataView(clone);
  const filter = view.getUint8(0);
  if (filter ===  0) return view;

  for (let i = 1; i < clone.byteLength; i++) {
    const currentPixel = view.getUint8(i);
    const prevPixel = i >= pixelLength + 1 ? view.getUint8(i - pixelLength) : 0;
    const abovePixel = previous ? previous.getUint8(i) : 0;
    const diagPixel = previous && i >= pixelLength + 1 ? previous.getUint8(i - pixelLength) : 0;

    if (filter === 1) {
      view.setUint8(i, currentPixel + prevPixel);
    } else if (filter === 2) {
      view.setUint8(i, currentPixel + abovePixel);
    } else if (filter === 3) {
      view.setUint8(i, currentPixel + Math.floor((abovePixel + prevPixel) / 2));
    } else if (filter === 4) {
      view.setUint8(i, currentPixel + filter4(
        prevPixel,
        abovePixel,
        diagPixel
      ));
    }
  }

  return view;
}

RawChunk.exclude(ChunkType.IDAT);
module.exports = ImageDataChunk;