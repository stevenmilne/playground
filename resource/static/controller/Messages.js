
const publicChannel = new BroadcastChannel('public');
const privateChannel = new MessageChannel();

privateChannel.port1.onmessage = function (event) {
  console.log(event);
}

publicChannel.postMessage('Client connected');
publicChannel.onmessage = function (event) {
  console.log(event);
}

publicChannel.postMessage({ event: 'connect', port: privateChannel.port2 });
