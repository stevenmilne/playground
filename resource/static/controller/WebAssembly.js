const { bytes2string, string2bytes } = await require('/module/util.js');

async function getModule(name, callback) {
	const module = await require(name);
	const memory = new WebAssembly.Memory({ initial: 1 });

	const instance = await WebAssembly.instantiate(module, {
		js: {
			mem: memory,
			log: (offset, length) => console.log(bytes2string(memory, offset, length))
		}
	});

	callback(instance, memory);
}

getModule('/wasm/hello.wasm', (instance, memory) => {
	instance.exports.hello();
});

getModule('/wasm/greet.wasm', (instance, memory) => {
	string2bytes(memory, 100, 'Steven');
	instance.exports.greet(100, 7);
});
