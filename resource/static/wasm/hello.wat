(module
  (import "js" "log" (func $log (param i32 i32)))
  (import "js" "mem" (memory 1))
  (data (i32.const 0) "Hello world, from WASM!")
  (func (export "hello")
    i32.const 0  ;; pass offset 0 to log
    i32.const 23  ;; pass length 23 to log
    call $log
	)
)
