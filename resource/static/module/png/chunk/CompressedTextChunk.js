const { readFields, TextReader, NumberReader, DataReader } = await require('/module/png/FieldReader.js');
const { Chunk, ChunkType } = await require('/module/png/chunk/Chunk.js');
const RawChunk = await require('/module/png/chunk/RawChunk.js');
const { inflate } = await require('/module/png/zlib.js');

class CompressedTextChunk extends Chunk {

  static async decode(chunk) {
    chunk.assert(ChunkType.zTXt);

    const [ key, method, data ] = readFields(chunk.bytes, [
      new TextReader(79),
      new NumberReader(),
      new DataReader()
    ])

    const decompressed = await inflate(data);
    const value = new TextDecoder().decode(decompressed);

    return new CompressedTextChunk(chunk.type, { key, value });
  }

  constructor(type, { key, value }) {
    super(type);

    this.key = key;
    this.value = value;
  }

  /**
   * @override
   * This function is implemented for typed chunks and returns a flag
   * which specifies if multiple chunks in the same file are supported.
   * @return {boolean} True if this chunk type supports duplicate chunks.
   */
  isStackable() {
    return true;
  }

  /**
   * @override
   * This function is implemented for stackable typed chunks. Chunks which
   * support being mapped by a keyword can implement this function to return
   * the unique keyword that can identify a chunk.
   * @return {string} A value that can map this chunk instance.
   */
  getKey() {
    return this.key;
  }
}

RawChunk.register(ChunkType.zTXt, CompressedTextChunk);
module.exports = CompressedTextChunk;
