this.addEventListener('install', event => {
  event.waitUntil(
    caches.open('v1').then(cache => {
      return cache.addAll([
        '/document.css',
        '/script/require.js',
        '/script/require-extensions.js'
      ]);
    })
  );
});

this.addEventListener('fetch', event => event.respondWith(
  caches.match(event.request).then(response => {
    if (response !== undefined) {
      return response;
    }

    return fetch(event.request).then(content => {
      caches.open('v1').then(cache => {
        cache.put(event.request, content.clone());
      })

      return content;
    });
  })
));
